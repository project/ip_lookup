<?php

namespace Drupal\ip_lookup\Resource;

/**
 * Base environment class.
 */
class Environment {
  /**
   * The base URL for the IP data API.
   *
   * @var string
   */
  protected static $urlStem = "https://api.ipdata.co";

  /**
   * Get base URL.
   *
   * @return string
   *   The base URL.
   */
  public static function getUrlStem() {
    return self::$urlStem . '/';
  }

}
